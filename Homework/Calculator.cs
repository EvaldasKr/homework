﻿using Homework.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Homework
{
    public class Calculator
    {
        public string specialChar = @"\|!#$%&/()=?»«@£§€{@};'<>";
        public string filePath;
        public List<TransanctionLog> log = new List<TransanctionLog>();

        public Calculator(bool startApp = true)
        {
            filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "transactions.txt");
            if (CheckIfFileExists(filePath))
            {
                Print(string.Format("File found in : {0} ", filePath));
                ReadFile(filePath);
                if (startApp)
                    Console.ReadLine();
            }
            else
            {
                Print(string.Format("File {0} not found ", filePath));
                if (startApp)
                    Console.ReadLine();
            }
        }

        /// <summary>
        /// Checks if file exists in specified directory
        /// </summary>
        /// <param name="path">complete path to file</param>
        /// <returns>true if file exists</returns>
        public bool CheckIfFileExists(string path)
        {
            if (!string.IsNullOrEmpty(path) && !string.IsNullOrWhiteSpace(path))
                return File.Exists(path);
            return false;
        }

        public bool CheckIfFileNotEmpty(string path) => new FileInfo(path).Length > 0;

        /// <summary>
        /// reads file line by line
        /// </summary>
        public void ReadFile(string path)
        {
            if (CheckIfFileNotEmpty(path))
            {
                Print("Reading file", true);
                int counter = 0;
                string line;
                StreamReader file = new StreamReader(path);
                while ((line = file.ReadLine()) != null)
                {
                    Print(ConvertTextLine(line, counter));
                    counter++;
                }
            }
        }

        /// <summary>
        /// converts text line to output line
        /// </summary>
        /// <param name="line">text to check and calculate transaction fee</param>
        /// <param name="lineNo">line number purelly for debugging reasons (COULD BE REMOVED)</param>
        /// <returns>output line, formated string for output</returns>
        public string ConvertTextLine(string line, int lineNo = 0)
        {
            string formatedString = "";
            if (!string.IsNullOrEmpty(line))
            {
                line = RemoveSpecialCharacters(line).Trim();
                string[] splitLine = line.Split(null);
                if (splitLine.Count() > 1)
                {
                    ErrorEnum error = ErrorEnum.none;

                    string dateFormated = "";
                    string customer = "";
                    string transactionFee = "";

                    foreach (var item in splitLine)
                    {
                        if (string.IsNullOrEmpty(dateFormated))
                            if (!DateTime.TryParse(item, out DateTime date))
                                error = ErrorEnum.date;
                            else
                                dateFormated = date.ToString("yyyy-MM-dd");
                        else if (string.IsNullOrEmpty(customer))
                            customer = item;
                        else if (string.IsNullOrWhiteSpace(item))
                            customer += " ";
                        else if (string.IsNullOrEmpty(transactionFee))
                            if (!double.TryParse(item, out double transactionAmount))
                                error = ErrorEnum.Transaction;
                            else
                                transactionFee = CalculateFee(transactionAmount, customer, DateTime.Parse(dateFormated));
                    }
                    //if (!error.Equals(ErrorEnum.none))  //used for debugging purposes
                    //    formatedString = string.Format("Could not read \"{0}\" in line: \"{1}\"; line text: \"{2}\"", Enum.GetName(typeof(ErrorEnum), error).ToUpper(), lineNo, line);
                    //else
                    formatedString = string.Format("{0} {1} {2}", dateFormated, customer, transactionFee);
                }
            }
            return formatedString;
        }

        /// <summary>
        /// Calculating Transaction fee
        /// </summary>
        /// <param name="transanction"> transaction amount</param>
        /// <param name="customer"> customer name used for logging</param>
        /// <param name="date"> transaction date used for adding initial fee 29</param>
        /// <returns></returns>
        public string CalculateFee(double transanction, string customer, DateTime date)
        {
            double transactionFee;
            transactionFee = transanction * 0.01;
            customer = customer.Trim();

            if (customer.Equals("TELIA"))
                transactionFee = transactionFee * 0.9;
            else if (customer.Equals("CIRCLE_K"))
                transactionFee = transactionFee * 0.8;

            if (transactionFee > 0)
            {
                var transaction = log.Where(item => item.customer == customer)
                    .OrderByDescending(t => t.date)
                    .FirstOrDefault();

                if (transaction != null)
                {
                    if (transaction.date.Month == (date.Month - 1) && transaction.fee > 0)
                        transactionFee += 29;
                }
                else
                    transactionFee += 29;
            }

            log.Add(new TransanctionLog { customer = customer, date = date, fee = transactionFee });

            return transactionFee.ToString("F");
        }

        /// <summary>
        /// Cleaning text line of specified special characters
        /// </summary>
        /// <param name="text"> line to clean</param>
        /// <returns>cleaned line</returns>
        public string RemoveSpecialCharacters(string text)
        {
            foreach (var chr in specialChar)
            {
                text = text.Replace(chr.ToString(), "");
            }
            return text;
        }

        /// <summary>
        /// Outputs text to console window
        /// </summary>
        /// <param name="message">message to output</param>
        /// <param name="lineSkip">add empty line after message</param>
        public void Print(string message, bool lineSkip = false)
        {
            //if (!string.IsNullOrEmpty(message)) && !string.IsNullOrWhiteSpace(message) && !lineSkip)  // this is for removing empty lines
            Console.WriteLine(DateTime.Now.ToString("HH:mm:ss") + " || " + message);
            if (lineSkip)
                Console.WriteLine(DateTime.Now.ToString("HH:mm:ss") + " || ");
        }

        //Could be extended for more precise errors
        public enum ErrorEnum
        {
            Transaction,
            date,
            none
        }
    }
}
