using Homework;
using NUnit.Framework;

namespace Tests
{
    public class Tests
    {
        Calculator calculator;

        [SetUp]
        public void Setup()
        {
            calculator = new Calculator(false);
            calculator.log = new System.Collections.Generic.List<Homework.Models.TransanctionLog>();
        }

        [Test]
        public void CalculatorTest()
        {
            new Calculator(false);
        }

        [Test]
        public void PrintTest()
        {
            calculator.Print("test");
            calculator.Print("");
            calculator.Print(" ");
            calculator.Print("test", true);
        }

        [Test]
        public void CheckIfFileExists()
        {
            Assert.IsTrue(calculator.CheckIfFileExists(calculator.filePath));
        }

        [Test]
        public void CheckIfFileNotEmpty()
        {
            Assert.IsTrue(calculator.CheckIfFileNotEmpty(calculator.filePath));
        }

        [Test]
        public void ReadFileTest()
        {
            calculator.ReadFile(calculator.filePath);
        }

        [Test]
        public void ConvertTextLineTest()
        {
            string convertedLine = calculator.ConvertTextLine("|2018-09-01 7-ELEVEN 100");

            Assert.AreEqual("2018-09-01 7-ELEVEN 30.00", convertedLine);
        }

        [Test]
        public void CalculateFeeTest()
        {
            string fee = calculator.CalculateFee(100, "7-ELEVEN", new System.DateTime(2019, 9, 1));

            Assert.AreEqual("30.00", fee);
        }

        [Test]
        public void RemoveSpecialCharactersTest()
        {
            string text = calculator.RemoveSpecialCharacters("|2018-09-01 7-EL@@EVEN 30.00");

            Assert.AreEqual("2018-09-01 7-ELEVEN 30.00", text);
        }
    }
}